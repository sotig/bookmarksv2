from django.contrib import admin
from supervisor.models import *
from modeltranslation.admin import TranslationAdmin, TabbedTranslationAdmin, TranslationStackedInline, TranslationTabularInline 

 
@admin.register(Url)
class UrlAdmin(admin.ModelAdmin): 
    search_fields = ('title', 'path')
    list_display = ('title', 'path')
 