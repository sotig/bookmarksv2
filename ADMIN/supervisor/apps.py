from django.apps import AppConfig


class SupervisorConfig(AppConfig):
    name = 'supervisor'
    verbose_name = "Supervisor"
