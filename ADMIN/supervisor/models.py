from djongo import models
from django import forms 
import datetime
# Create your models here.
  

class Url(models.Model):
    _id = models.ObjectIdField()

    title = models.CharField(max_length=255, blank=True, null=True)
    url = models.CharField(max_length=255, blank=True, null=True) 
    thumb = models.CharField(max_length=255, blank=True, null=True) 
    path = models.CharField(max_length=2048, blank=True, null=True)

    lastThumb = models.DateTimeField() 
    createdAt = models.DateTimeField() 

    clicks = models.IntegerField(default=0)
 
    objects = models.DjongoManager()
    
    def __str__(self):
        return str(self.title)
    
    class Meta:
        verbose_name_plural = "Urls"
        verbose_name = "Url"
        db_table = 'url'
  