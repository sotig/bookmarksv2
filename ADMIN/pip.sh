#!/bin/bash 
if [[ "$OSTYPE" == "win32" ]]; then
    pip install Django==1.11.16
    pip install djongo 
    pip install django-filter  # Filtering support
    pip install django-jet   
    pip install django-cors-headers
    pip install django-pipeline
    pip install django-tabbed-admin
    pip install Pillow
    pip install django-modeltranslation==0.13b1
    pip install gunicorn
else
    pip3 install Django==1.11.16
    pip3 install djongo 
    pip3 install django-filter  # Filtering support
    pip3 install django-jet   
    pip3 install django-cors-headers
    pip3 install django-pipeline
    pip3 install django-tabbed-admin
    pip3 install Pillow
    pip3 install django-modeltranslation==0.13b1
    pip3 install gunicorn 
fi