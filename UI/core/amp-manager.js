let _isAmp = false;
let _ampUrl = "";


module.exports = class AmpManager{
 
  static set(req, query){
    const amp = query.amp == '1'
  
    const url = req ? req.url : window.location.href
    const ampUrl = amp ? url.replace('?amp=1', '') : url + '?amp=1'
    _isAmp = amp;
    _ampUrl = ampUrl;

    return { amp, ampUrl }
  }
  static get(){
    const amp = _isAmp; 
    const ampUrl = _ampUrl;
    return { amp, ampUrl }
  }
} 