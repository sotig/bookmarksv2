
const puppeteer = require('puppeteer');

const dev = process.env.NODE_ENV !== 'production'
var fs = require('fs');

var sleep = require('./sleep')


function getUrlName(url) {
  let path = url
  path = url.replace(/https:\/\//gi, '').replace(/http:\/\//gi, '').replace(/www\./gi, '').replace(/\//gi, '!').replace(/\?/gi, '!')

  if (path.endsWith('!')) {
    path = path.substring(0, path.length - 1);
  }

  return path
}


module.exports = async function saveSnapShotAndTitle(url) {
  let browser = await puppeteer.launch({
    // executablePath: '/usr/bin/chromium-browser',
    headless: true,
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
    ]
  });

  const page = await browser.newPage();
  const cookies = [{
    'url': url,
    'name': '__cfduid',
    'value': 'd49177e7881e95e939da3b20447c0f52e1562617791'
  }, {
    'url': url,
    'name': 'b',
    'value': '159771ee-e0a9-40fe-87c1-dd34fc306066'
  }, {
    'url': url,
    'name': 'a',
    'value': 'd0b22a1b-2edd-42cd-a6af-234e42166feb'
  }];

  await page.setCookie(...cookies);

  page.setDefaultTimeout(60000)

  await page.setViewport({ width: 360, height: 360 });
  await page.goto(url);

  if (!fs.existsSync("/media/thumbs/")) {
    fs.mkdirSync("/media/thumbs/");
  }

  await sleep(10000)
  await page.screenshot({ path: "/media/thumbs/" + getUrlName(url) + ".jpg" });

  let title = await page.$eval('title', e => e.text);

  browser.close()
  browser = null

  return {
    thumb: "media/thumbs/" + getUrlName(url) + ".jpg",
    title: title
  }
}