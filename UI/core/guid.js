var md5 = require('md5');
export default class Guid{
    static create(){
        function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }
    static createHash(){
        return md5(Guid.create());
    }
}
