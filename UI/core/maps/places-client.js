import $ from "jquery";
import Guid from "../guid";
import GoogleMapsLoader from "./GoogleMapsLoader"

export default class PlacesClient{
 
  load(){
    return new Promise(async resolve=>{ 
      await GoogleMapsLoader.ensureLoaded();
 
      this._context = new google.maps.places.PlacesService(document.createElement("DIV"));
      resolve(this);  
      
    })
  }

  /**
   * 
   * @param {*} query Search query
   * @param {*} fields Fields to be included in the resaults
   * @param {*} extra Extra parameters read more at https://developers.google.com/maps/documentation/javascript/places
   */
  async find(query, fields, extra){
    if (!extra)
      extra = {};

    extra["query"] = query
    extra["fields"] = fields

    return new Promise(resolve=>{
      this.service.findPlaceFromQuery(extra, (results, status) =>{
        resolve(results);
      });
    })

  }

  /**
   * 
   * @param {*} locationQuery Search center point
   * @param {*} radius Search circle radius in meters
   * @param {*} placeQuery Place search query
   * @param {*} type Search filtering by type
   * @param {*} extra Extra parameters read more at https://developers.google.com/maps/documentation/javascript/places
   */
  async findNearby(locationQuery, radius, placeQuery, type, extra){
    if (!extra)
      extra = {};
 
    extra["radius"] = radius
    extra["type"] = type
    extra["query"] = placeQuery
    extra["fields"] = ["name"]
    let places = await this.find(locationQuery, ['geometry'] );
    
    if (places) 
      extra["location"] = new google.maps.LatLng(places[0].geometry.location.lat(), places[0].geometry.location.lng()); 
    
    return new Promise(resolve=>{
      this.service.textSearch(extra, (results, status) =>{
        resolve(results);
      }); 
    })
    
  }

  /**
   * 
   * @param {number} lat
   * @param {number} lng 
   * @param {number} radius 
   * @param {string} placeQuery 
   * @param {array} type 
   * @param {object} extra 
   */
  async findNearbyPlace(lat, lng, radius, placeQuery, type, extra){
    if (!extra)
      extra = {};
 
    extra["radius"] = `${radius}`
    extra["type"] = type
    extra["query"] = placeQuery 
    extra["location"] = new google.maps.LatLng(lat, lng);
    
    return new Promise(resolve=>{
      this.service.textSearch(extra, (results, status) =>{ 
        resolve(results);
      }); 
    })
    
  }

  get service(){
    return this._context;
  }
}