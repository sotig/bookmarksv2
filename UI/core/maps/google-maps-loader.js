import $ from 'jquery'
import uiSettings from "../../ui.settings"
let mapsKey = null;
let langauage = null;

export default class GoogleMapsLoader{
  static ensureLoaded(){
    mapsKey = uiSettings.googleMapsKey;
    langauage = uiSettings.googleMapsLanaguage;
    return new Promise(resolve=>{ 
      if (document.mapsLoaded == undefined || document.mapsLoaded == false){

        document.mapsLoaded = true;
        let libs = uiSettings.googleMapsLibraries;
        let libsAttr = "";

        libs.forEach(lib=>{
          libsAttr += lib + ",";
        });

        if (libsAttr.length>0)
          libsAttr = libsAttr.substring(0, libsAttr.length - 1);
          
        $("head").append(/*html*/`
          
          <script>
            function initGoogleMaps() {
              document.mapsReady = true;
              var event = document.createEvent("Event");
              event.initEvent("mapsLoaded", false, true);  
              document.dispatchEvent(event);
            }
          </script>

          <script async defer type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=${mapsKey}&libraries=${libsAttr}&language=${langauage}&callback=initGoogleMaps"></script>
        
        `)
        document.addEventListener("mapsLoaded", ()=>{
          resolve();
        });
      } 
      else{
        if (document.mapsReady)
          resolve();
        else
          document.addEventListener("mapsLoaded", ()=>{
            resolve();
          });
      }
    });
  }
  static get key(){
    return mapsKey;
  }
}