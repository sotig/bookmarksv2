

let _contx = {};

export default class ContextSummoner{

    static Invoke(ctx){
        return new Promise(async resolve => {
            _contx.authenticated = ctx.req.authenticated;
            _contx.user = ctx.req.user;
                
            resolve(_contx)
        });
    }

    static get get(){
        return _contx;
    }
    static set set(value){
        _contx = value;
    }
}