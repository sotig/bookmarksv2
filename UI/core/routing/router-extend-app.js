let PageRoutes = require("./page-routes")
const globals = require("../../globals")
const { MongoClient } = require('mongodb') 
const auth = require("../../actions/auth")

module.exports =  function routerExtendApp(app) {

  app.createPath =  (pattern, actualPage, customHandler, options) => {
   
    PageRoutes.add(pattern, actualPage);
    return {
      pattern: pattern,
      handler: async (req, res) => {
        const client = await MongoClient.connect(globals.dbUrl + globals.dbName, { useNewUrlParser: true });
        req.db=client.db(globals.dbName)
        req.page_routes = PageRoutes.get();
        let { authenticated, user, session } = await auth(req, res, req.db);
        req.authenticated = authenticated;
        req.user = user;
        req.session = session;

        if (customHandler)
          customHandler(req, res)
 
        req.allowAmp = true;
        if (options)
        {
          if ( options.allowAmp==false ){
            req.allowAmp = false;
          } 
        }
 
        app.render(req, res, `/${actualPage}`, req.query)
      },
    }
  }
}