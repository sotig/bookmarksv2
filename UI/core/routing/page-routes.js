let page_routes = {}

module.exports = class PageRoutes{
 
  static add(pattern, name){
    page_routes[name] = pattern;
  }
  static get(){
    const routes = page_routes
    return routes
  }
  static set(routes){
    page_routes = routes;
  }
} 