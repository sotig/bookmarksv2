import AmpManager from "../../core/amp-manager"; 
import PageRoutes from "../../core/routing/page-routes";

export default class Link extends React.Component{
   
 
  render(){
    let {amp, ampUrl} = AmpManager.get(); 
    let href = "";
    if (this.props.page)
      href = PageRoutes.get()[this.props.page];
    else
      href = this.props.url;
  
    return (
      <a 
        href={amp?href+"?amp=1":href} 
        className={this.props.className}
        style={this.props.style} 
        title={this.props.title}
        target={this.props.target}
        rel={this.props.rel}>
          {this.props.children} 
      </a>);
  }
}