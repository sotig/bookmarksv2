

import css from "./styles.scss"
import itemcss from "./item.scss"
import TextBox from "../TextBox"
import $ from "jquery"

import Spinner from "../Spinner"
import moment from "moment"
import PlacesClient from "../../core/maps/PlacesClient"
import uiSettings from "../../ui.settings" 

export default class PlacesSearch extends React.Component {
  
  constructor(props){

    super(props);

    this.state = {
      items: <div></div>
    }
  
    this._category = this.props.category; 
    this.timeout = moment();
    this.latency = 500;
    this.allow = true; 
    this._selectedItem = null;
    this._mouseInDropDown = false;

  }
 
  clearAndLoading(){
    this.setState({ 
      items: <div className={css.loading_spinner}><Spinner></Spinner></div>
    })
  }

  handleInput(){
  
    this.timeout = moment();

    this._selectedItem = null;
     
    this.clearAndLoading();

    if (this.refs.textbox.value != ""){
      this.openDropDown();
    }
    else{
      this.closeDropDown();
    }

  }

  handleSearch(){

    setTimeout(()=>{
      this.allow = moment().diff(this.timeout)>this.latency

      if (this.allow)
        if (this.refs.textbox.value.trim != "")
          this.fetch();
      else
        this.clearAndLoading(); 

    }, this.latency+50);

  }

  openDropDown(){
    this.refs.dropdown.classList.add(css.dropdown_show);
  }

  closeDropDown(){
    this.refs.dropdown.classList.remove(css.dropdown_show); 
  }

  itemSelected(item){
    this._selectedItem = item;
    this.refs.textbox.value = item.name;
    this.closeDropDown();
    $(this.refs.body).trigger("selectionChanged");
  }
  
  async fetch(){
 
    let places = await this.places.findNearbyPlace(uiSettings.athensCenter.lat, uiSettings.athensCenter.lng, 15000, this.refs.textbox.value, [this.props.type]);
    let cn = 0;
    if (places.length > 0)
      this.setState({
        items: places.map(place=>{
          return <SearchItem key={cn++} onClick={ (item)=>{ this.itemSelected(item) } } place={place}></SearchItem>
        })
      })
    else
      this.setState({ 
        items: <div className={css.loading_spinner}>Δεν βρέθηκαν αποτελέσματα</div>
      })

  }

  async componentDidMount() { 
    
    this._selectedItem = null;

    this.clearAndLoading();

    this.places = await new PlacesClient().load();
     
    $(this.refs.textbox.refs.input).on("input", ()=>{  
      this.handleInput(); 
    });

    $(this.refs.textbox.refs.input).on("keyup", ()=>{  
      this.handleSearch(); 
    });

    $(this.refs.dropdown).mouseenter(()=>{
      this._mouseInDropDown = true;
    });

    $(this.refs.dropdown).mouseleave(()=>{
      this._mouseInDropDown = false;
    });

    $(this.refs.textbox.refs.input).focusout(()=>{  
      if (!this._mouseInDropDown && this._selectedItem == null){ 
        this.refs.textbox.value = "";
        this.closeDropDown(); 
        $(this.refs.body).trigger("selectionChanged");
      }
    });
  }

  get value(){
    return this._selectedItem;
  }

  set value(place){
    this._selectedItem = place;
    if (place != null)
      this.refs.textbox.value = place.name;
  }

  removeError(){
    this.refs.textbox.removeError();
  }

  error(){ 
    this.refs.textbox.error();
  }

  render() { 

    return (
      <div className={css.wraper} ref="body">
        <TextBox ref="textbox" placeholder={this.props.placeholder}></TextBox>
        <div className={css.dropdown} ref="dropdown"> 
            {this.state.items} 
        </div>
      </div>
    );

  }

}

class SearchItem extends React.Component{
  constructor(props){
    super(props); 
    this.handleClick = this.handleClick.bind(this); 
  }

  handleClick() {
    if (!this.disabled)
      this.props.onClick(this.props.place);
  }
    
  render(){
    return (
      <div onClick={this.handleClick} className={itemcss.body}>
      <div className={itemcss.name}>{this.props.place.name}</div>
      <div className={itemcss.address}>{this.props.place.formatted_address}</div> 
      </div>
    );
  }
}