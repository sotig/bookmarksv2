import css from "./styles.scss";
import AllCenter from '../AllCenter';

export default class extends React.Component{
    render(){
        return(
        <a className={css.a} href={this.props.url} >
            <div className={css.tile}>
                    <div className={css.thumb}>
                        <AllCenter>
                            <i className="fas fa-folder"></i>
                        </AllCenter>
                    </div>
                    <div className={css.title}>
                        {this.props.name}
                    </div>
            </div>
        </a>
        )
    }
}