import css from "./styles.scss";

export default class extends React.Component{

    render(){
        return (
            <div className={css.wrapper}>
                <form action="/login" method="post">
                    <div>
                        <input type="text" name="username" placeholder={'Username'} autoFocus className={css.textbox} />
                    </div>
                    <div>
                        <input type="password" name="password" placeholder={'Password'} className={css.textbox} />
                    </div>
                    <div>
                        <input type="submit" value="Login" className={css.button} />
                    </div>
                </form>
            </div>
        );
    }

}