import css from "./styles.scss"  

export default class Spinner extends React.Component {
    
  render() {
    return (
      <div className={css.lds_ring}>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    );
  } 

}

