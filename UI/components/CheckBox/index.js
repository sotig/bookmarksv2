import css from "./checkbox.scss"  

export default class CheckBox extends React.Component {
    
  constructor(props){
    super(props);
    this._active = false;
  } 
  handleClick(){
    this.refs.wrapper.classList.remove(css.error);
    if (this._active)
    {
      this._active =false;
      this.refs.wrapper.classList.remove(css.toggle);
    }
    else{
      this._active = true;
      this.refs.wrapper.classList.add(css.toggle);
    }
  }
  clickBox(){
    this.handleClick();
  }
  componentDidMount(){
    this.clickLabel();
  }
  clickLabel(){
    let spans = this.refs.label.querySelectorAll("span");
    let cn = spans.length;
    for (let i =0; i<cn;i++){
      spans[i].onclick = ()=>{
        this.handleClick();
      }
    }
  }
  error(){
    this.refs.wrapper.classList.add(css.error);
  }

  get checked(){
    return this._active;
  }

  render() {
    return (
      <div className={css.wrapper} ref="wrapper"> 
        <table>
          <tbody>
            <tr>
              <td className={css.box_cell} onClick={()=>this.clickBox()}>
                <div className={css.box}>
                  <div className={css.checkmark}></div>
                </div>
              </td>
              <td className={css.cell_text} ref="label">
                <div className={css.text}>{this.props.children}</div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  } 

}

