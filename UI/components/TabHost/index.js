import css from "./styles.scss"  

export default class extends React.Component {
    
  set active(value){
    if (value){
      this.refs.body.classList.add(css.active)
    }
    else{ 
      this.refs.body.classList.remove(css.active)
    }
  }
  render() {
    return (
      <div className={css.tabbtn} ref="body">
        {this.props.children}
      </div>
    );
  } 

}

