import css from "./styles.scss";
import $ from "jquery";
import BodyEnd from "../BodyEnd"; 
import sleep from "../../core/sleep"; 

let showAnimation = null;
let hideAnimation = null;
 
export default class FullScreen extends React.Component {
  state = {
    render: this.props.render ? this.props.render : false,
  };

  isOpen() {
    return this.state.render? true: false
  }

  fadeIn() {
    return new Promise(async (resolve) => {
      // if (!this.overlay) {
      //   resolve();
      //   return;
      // }

      // if (!this.props.animate) {
      //   resolve();
      //   return;
      // }
 
      this.overlay.classList.add(css.show)
      resolve()

      // if (showAnimation) {
      //   showAnimation.pause();
      // }

      // showAnimation = anime({
      //   targets: this.overlay,
      //   duration: 300,
      //   easing: "linear",
      //   opacity: [0, 1],
      //   // scale: [1.1, 1],
      //   complete: async () => {
      //     resolve();
      //   },
      // });
    });
  }

  fadeOut() {
    return new Promise(async (resolve) => {
      // if (!this.overlay) {
      //   resolve();
      //   return;
      // }
 
      this.overlay.classList.remove(css.show)  
      await sleep(100);
      resolve()
      // if (hideAnimation) {
      //   hideAnimation.pause();
      // }
      // hideAnimation = anime({
      //   targets: this.overlay,
      //   duration: 300,
      //   easing: "linear",
      //   opacity: [this.overlay.style.opacity, 0],
      //   // scale: [1, 1],
      //   complete: async () => {
      //     resolve();
      //   },
      // });
    });
  }

  fullHeight() { 
    $(this.overlay).height(window.innerHeight || 0);
    this.sizeInterval = setInterval(() => {
      $(this.overlay).height(window.innerHeight || 0);
    }, 100); 
  }

  async show() {
    await this.setState({
      render: true,
    });

    this.fullHeight();
    if (this.props.onBeginShow) {
      this.props.onBeginShow();
    }

    if (this.props.onShow) {
      this.props.onShow();
    }

    this.fadeIn();

 
    if (!this.props.ignoreScroll) {
      $("html").css({
        overflowY: "hidden",
      });
    }
  }

  hide() {
    return new Promise(async (resolve) => {
   
      $("html").css({
        overflowY: "auto",
      });

      if (this.props.onBeginHide) {
        this.props.onBeginHide();
      }

      await this.fadeOut();

      await this.setState({
        render: false,
      });

      if (this.sizeInterval) {
        clearInterval(this.sizeInterval);
      }
      resolve();
    });
  }

  componentWillUnmount() {
    if (!this.props.ignoreScroll) {
      $("html").css({
        overflowY: "auto",
      });
    }
    if (this.sizeInterval) {
      clearInterval(this.sizeInterval);
    }
  }

  handleClick() {
    if (this.props.onClick) this.props.onClick();
  }

  mainStyle() {
    return {
      opacity: this.props.animate ? null : 1,
    };
  }

  render() {
    return this.state.render ? (
      <BodyEnd>
        <div
          ref={(r) => (this.overlay = r)}
          style={this.mainStyle()}
          className={css.wrapper}
          onClick={() => this.handleClick()}
        >
          {this.props.children}
        </div>
      </BodyEnd>
    ) : null;
  }
}
