import css from './textbox.scss' 
import $ from "jquery";   
import Guid from "../../core/guid"

export default class TextBox extends React.Component {
  constructor(props){
    super(props);
    this.guid = Guid.createHash();  
  }
  render() {
    return (
      <div className={`${css.textbox} ${this.props.className}`} ref="textbox">
        {
          (this.props.type=="multiline")? 
          <textarea name={this.guid} id={this.guid} ref="input" className={css.input} value={this.props.value}></textarea>
          :<input name={this.guid} id={this.guid} ref="input" type="text" className={css.input} value={this.props.value}/> 
        }
     
        <div className={css.line_bottom}></div> 
        <div className={css.line_focus}></div> 
        <div className={css.line_error}></div> 
        <label htmlFor={this.guid} className={(this.props.type=="multiline")?`${css.placeholder_top} ${css.placeholder}`:css.placeholder}>{this.props.placeholder}</label>
      </div>
    );
  }

  componentDidMount() { 
    $(this.refs.textbox).addClass(this.props.className);
    this.handleFocus(); 
    this.handleModeMax();
  }

  handleModeMax(){
    if (
      this.props.mode=="numerical" ||
      this.props.max != undefined){
      $(this.refs.input).keydown(e => { 
        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || 
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 39)) { 
                return;
        } 
         
        if (this.props.max != undefined)
          if (this.refs.input.value.length == parseInt(this.props.max))
              e.preventDefault();
              
        if (this.props.mode=="numerical")
          if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105))  
              e.preventDefault();  
      }); 
    }
  }

  handleFocus(){
    $(this.refs.input).focus(()=>{ 
      $(this.refs.textbox).addClass(css.textbox_focus);
      $(this.refs.textbox).addClass(css.placeholder_focus);
      $(this.refs.textbox).removeClass(css.textbox_error); 
      
    });
    $(this.refs.input).focusout(()=>{ 
      $(this.refs.textbox).removeClass(css.textbox_focus);
      if ($(this.refs.input).val().trim() == "")
        $(this.refs.textbox).removeClass(css.placeholder_focus);
    }); 
  }

  valueChangedFocus(){
    if ($(this.refs.input).val().trim() != "")
      $(this.refs.textbox).addClass(css.placeholder_focus);
    else
      $(this.refs.textbox).removeClass(css.placeholder_focus); 

    $(this.refs.textbox).removeClass(css.textbox_error); 
  } 

  error(){
    $(this.refs.textbox).addClass(css.textbox_error); 
  }
  removeError(){
    $(this.refs.textbox).removeClass(css.textbox_error); 
  }

  get value(){
    return this.refs.input.value;
  }
  
  set value(value){
    this.refs.input.value = value;
    this.valueChangedFocus();
  }

}

