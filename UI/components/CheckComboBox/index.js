import css from "./checkbox.scss"  
import $ from "jquery"

export default class CheckComboBox extends React.Component {
     
  constructor(props){
    super(props); 
    this._active = this.props.active!=undefined;
  }
  componentDidMount(){ 

    if (this.props.group!=undefined)
      this.listenToGroup(); 

  }

  handleClick(){
    if (this.props.group != undefined)
      this.toggleGroup();
    else
      this.toggle();
  }

  turnOn(){
    this._active = true;
    this.refs.wrapper.classList.add(css.toggle);
  }

  turnOff(){
    this._active =false; 
    this.refs.wrapper.classList.remove(css.toggle);
  }

  toggle(){
    if (this._active)
      this.turnOff();
    else
      this.turnOn();
  }

  toggleGroup(){
    $(document).trigger("check_combobox_group_event_" + this.props.group)
    this.turnOn();
  }
  listenToGroup(){ 
    $(document).on("check_combobox_group_event_" + this.props.group, ()=>{
      this.turnOff();
    });
  }

  error(){
    this.refs.wrapper.classList.add(css.error);
  }

  get checked(){
    return this._active;
  }

  render() {
    return (
      <div className={ `${css.wrapper} ${this.props.active!=undefined?css.toggle:""}` } ref="wrapper" onClick={()=>this.handleClick()}> 
        <div> 
          <div className={css.box_cell}>
            <div className={css.box}>
              <div className={css.checkmark}></div>
            </div>
          </div>
          <div className={css.box_cell}>
            <span className={css.text}>{this.props.children}</span>
          </div> 
        </div>
      </div>
    );
  } 

}

