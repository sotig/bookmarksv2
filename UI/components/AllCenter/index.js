export default class AllCenter extends React.Component {
    
  render() {
    return (
      <div style={ { position:"relative", width: "100%", height:"100%" } }>
        <div style={ {display: "table", position: "absolute", height: "100%", width: "100%" }}>
          <div style={{ display: "table-cell", verticalAlign: "middle" }}>
              <div style={{ marginLeft: "auto", marginRight: "auto", width:"100%" }}>
                  <div>
                    {this.props.children}
                  </div>
              </div>
          </div>
        </div> 
      </div>
    );
  } 

}

