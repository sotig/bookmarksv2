 
import AmpManager from "../../core/amp-manager"
import css from "./style.scss"
export default class Img extends React.Component {
    
  render() { 
    return (
      <span className={css.img}>
        {
          AmpManager.get().amp ? (
            <div style={ {display:"inline-block"} }>
              <amp-img   
                src={this.props.src} 
                alt={this.props.alt}
                title={this.props.title}
                width="360"
                height="100"
                layout="responsive"
                class={this.props.className} >
              </amp-img> 
            </div>
          ) : (
            <img  
              className={this.props.className}
              src={this.props.src} 
              alt={this.props.alt}
              title={this.props.title} /> 
          )
        }
      </span>
    
    );
  } 

}

