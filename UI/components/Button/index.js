import css from "./button.scss"  
import Spinner from "../Spinner"
export default class Button extends React.Component {
  
  constructor(props){
    super(props);
    this._disabled = false;
    this.handleClick = this.handleClick.bind(this);
    this.state = { content:this.props.children };
  }
   
  handleClick() {
    if (!this.disabled)
      this.props.onClick();
  }

  set disabled(value) {
    this._disabled=value;  
    if (value) {
      this.refs.body.classList.add(css.disabled);
    }
    else {
      this.refs.body.classList.remove(css.disabled);
    }
  }

  get disabled(){
    return this._disabled;
  }

  set loading(val){
    if (val==true)
      this.setState({
        content:<Spinner></Spinner>
      }); 
    else
      this.setState({
        content:this.props.children
      }); 
  }
  
  render() {
    return ( 
      <div className={css.form_button} onClick={this.handleClick} ref="body">

        {this.state.content}

      </div> 
    );
  } 
}

