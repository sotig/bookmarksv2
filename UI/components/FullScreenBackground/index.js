import css from './styles.scss'

export default class FullScreenBackground extends React.Component {
  handleClick() {
    if (this.props.onClick) {
      this.props.onClick()
    }
  }
  render = () => <div onClick={this.handleClick.bind(this)} className={`${this.props.className} ${css[this.props.type]}`}/>
}