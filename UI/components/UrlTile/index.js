import css from "./styles.scss";
import EllipsizeParagraph from "../EllipsizeParagraph";
import $ from 'jquery'

export default class extends React.Component {

  componentDidMount() {
    $(this.a).click(async (e) => {
      let href = this.a.href

      e.preventDefault()

      $.get(`/handle-click?id=${this.props.url._id}`);

      window.open(href, '_blank');
    })

    let fixHeight = () => {
      $(this.tile).css({
        height: $(this.tile).width()
      })
    }

    fixHeight()
    $(window).resize(() => {
      fixHeight()
    })

  }
  
  show_alert(e) {
    e.preventDefault();
    if(!confirm("Delete url?")) {
      return false;
    } 
    this.delform.submit();
  }

  render() {
    console.log(this.props.url)
    return (
      <div className={css.relative}>
        <form ref={r=>this.delform=r} onSubmit={this.show_alert.bind(this)} action={`/deleteurl/?path=${this.props.url.path}&id=${this.props.url._id}`} method="post"> 
          <button type="submit" className={css.delete_btn}> 
            <i className={`fas fa-times`} />
          </button>
        </form> 
        <a  href={this.props.url.url} target="_blank" ref={r => this.a = r}>
          <div className={css.outer_wrapper}>
            <div className={css.wrapper} >
              <div className={css.tile} ref={r => this.tile = r}>
                <div className={css.thumb}>
                  <img src={`/${this.props.url.thumb}`} />
                </div>
              </div>
              <div className={css.title}>
                <EllipsizeParagraph className={css.title_text}>
                  {this.props.url.title}
                </EllipsizeParagraph>
              </div>
            </div>
          </div>
        </a>
      </div>
    )
  }
}