
import css from "./styles.scss";

export default class EllipsizeParagraph extends React.Component {

  state = {
    show: false
  }

  ellipsizeTextBox(el) {
    var wordArray = el.innerHTML.split(' ');
    let cn = 0
    while (el.scrollHeight - 3 > el.offsetHeight && cn < wordArray.length) {
      wordArray.pop();
      el.innerHTML = wordArray.join(' ') + '...';
      cn ++;
    }
  }

  componentDidMount() {
    this.ellipsizeTextBox(this.wrapper);
    this.setState({
      show: true
    })
  }

  componentDidUpdate() {
    this.ellipsizeTextBox(this.wrapper);
  }

  render() {
    return (
      <div
        ref={r => this.wrapper = r}
        style={{ opacity: this.state.show ? "1" : "0" }}
        className={`${css.wrapper} ${this.props.className}`}>
        {this.props.children}
      </div>
    )
  }

}