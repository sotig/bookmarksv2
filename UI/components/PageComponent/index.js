import ContextSummoner from "../../core/context-summoner"
import React from "react"

export default class PageComponent extends React.Component{
    async componentWillMount(){  
        ContextSummoner.set = this.props.context;
    }
}