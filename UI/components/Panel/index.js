import css from "./styles.scss"  

export default class extends React.Component {
    
  render() {
    return (
      <div className={css.panel}>
        {this.props.children}
      </div>
    );
  } 

}

