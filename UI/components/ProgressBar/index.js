import css from "./styles.scss"
import anime from 'animejs' 

export default class ProgressBar extends React.Component {
  
  constructor(props){
    super(props);
    this._progress = this.props.progress ? parseFloat(this.props.progress).toFixed(2) : 0;
    this.state = {
      progress: parseInt(this._progress),
    }
  }

  componentDidMount(){
    this.drawProgress();
    anime({
      targets: this.refs.dot,
      translateX: [23, 25],
      translateY: [2, 2],
      scale: [.7,.7],
      easing: "linear",
      loop:true,
      direction: 'alternate',
      duration:1000,
    }); 
   
  }

  drawProgress(){
    let pBar = this.refs.progress;
    let progress = this._progress;

    let pAnimation = this._pAnimation? this._pAnimation : anime();
    pAnimation.pause();
  
    pAnimation = anime({
      targets: pBar,
      right: [pBar.style.right, `${100 - progress}%`],
      easing: "easeOutBack",
      update: () => {
        this.refs.text.textContent = 100 - parseInt( pBar.style.right ) + "%" 
      },
      duration:500
    }); 
   
    this._pAnimation = pAnimation;
  }

  set progress(value){
    this._progress = value;
    this.drawProgress();
  }

  get progress(){
    return this._progress;
  }

  render() {
    return (
      <div className={css.tray}>
        <div className={css.progress} ref="progress" style={ {right: `${100 - this._progress}%`} }>
          <div className={css.progress_text} ref="text">{this.state.progress}%</div>
          <div className={css.dots} ref="dot"></div>
        </div>
      </div>
    );
  } 

}

