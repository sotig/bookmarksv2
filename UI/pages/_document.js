import React, { Component } from 'react'
import Document, { Head, Main, NextScript } from 'next/document'
import PropTypes from 'prop-types'
import flush from 'styled-jsx/server'
var fs = require('fs');  
import settings from "../globals" 
import AmpManager from "../core/amp-manager.js" 
import PageRoutes from "../core/routing/page-routes"
 
 export default class HybridDocument extends Document {
  static getInitialProps ({req, query, renderPage }) {
    
    if (req.allowAmp)
      if (settings.useAmp)
        AmpManager.set(req, query);

    const { html, head, errorHtml, chunks } = renderPage()
    const styles = flush()
    let amp = false

    head.forEach(element => {
      if (element.type == 'link' && element.props.rel == 'canonical') {
          amp = true
      }
    }) 
    PageRoutes.set(req.page_routes); 
    return { html, head, errorHtml, chunks, styles, amp }
  }

  static childContextTypes = {
    _documentProps: PropTypes.any
  }
 
  getChildContext () {
    return { _documentProps: this.props }
  }

  render () {
    const {amp, ampUrl} = AmpManager.get(); 
    
    if (amp && settings.useAmp) {
      return <html amp="">
        <AmpHead >
          <link rel="canonical" href={ampUrl} />
          <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />   
          <meta name="viewport" content="minimum-scale=1.0, initial-scale=1.0, width=device-width" key="viewport" />
          </AmpHead>
        <body>
          
          <Main />
        </body>
      </html>
    } else {
      return <html>
        <Head> 

          {
            !amp && settings.useAmp?
            (<link rel="amphtml" href={ampUrl} />):
            (<meta />)
          }  
          
          <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />   
          <meta name="viewport" content="minimum-scale=1.0, initial-scale=1.0, width=device-width" key="viewport" />
          <link href="/static/fontawesome/css/all.min.css" rel="stylesheet" />
        </Head>
        <body>
          <Main />
          <NextScript />  
        </body>
      </html>
    }
  }
}


export class AmpHead extends Component {
  static contextTypes = {
    _documentProps: PropTypes.any
  }
  readStyles(path){  
    return fs.readFileSync(path, "utf-8"); 
  }
  render () {
    const { head, styles, __NEXT_DATA__ } = this.context._documentProps
    const { pathname, buildId, assetPrefix, nextExport } = __NEXT_DATA__
    const pagePathname = getPagePathname(pathname, nextExport)
  
    return <head {...this.props}>
 
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto' /> 
    <style amp-boilerplate=''>{`body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}`}</style><noscript><style amp-boilerplate=''>{`body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}`}</style></noscript>
    <script async src='https://cdn.ampproject.org/v0.js' />
    {(head || []).map((h, i) => React.cloneElement(h, { key: i }))}
    {styles || null}
    {this.props.children}  
    <style amp-custom="" dangerouslySetInnerHTML={ {__html: this.readStyles(".next/static/css/styles.chunk.css")} }></style>
    </head>
  }
}
 //await this.readStyles("/_next/static/css/styles.chunk.css")
function getPagePathname (pathname, nextExport) {
  if (!nextExport) return pathname
  if (pathname === '/') return '/index.js'
  return `${pathname}/index.js`
}