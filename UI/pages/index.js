
import css from "./scss/index.scss"
import Layout from './layout.js'
import Head from 'next/head'
import Img from "../components/Img";

import Link from "../components/Link";

import PageRoutes from "../core/routing/page-routes";
import Panel from "../components/Panel";
import TabButton from "../components/TabButton";
import ContextSummoner from "../core/context-summoner";
import PageComponent from "../components/PageComponent";
import UrlTile from "../components/UrlTile";
import FolderTile from "../components/FolderTile";
import FullScreen from "../components/FullScreen"
import FullScreenBackground from "../components/FullScreenBackground";

export default class extends PageComponent {

  static async getInitialProps(ctx) {
    const db = ctx.req.db;

    if (!ctx.req.query.path)
      ctx.res.redirect("/?path=/")

    let path = ctx.req.query.path

    let context = await ContextSummoner.Invoke(ctx);
    let urls = [];
    let subDirs = [];
 
      if (path == "/") {
        urls = await db.collection("url").find({ $or: [{ path: ctx.req.query.path }, { path: "" }, { path: null }] }).toArray();
      }
      else {
        urls = await db.collection("url").find({ path: ctx.req.query.path }).toArray();
      }


      subDirs = (
        await db.collection("url")
          .distinct("path",
            {
              path: { $regex: new RegExp(`^${path}\/?[^\/]*$`, 'g'), $ne: path }
            },
            { $sort: { path: 1 } },
            {
              _id: 0,
              path: 1
            }
          )
      )
 
  
    return { context, urls, path, subDirs };
  }

  componentDidMount() {
    //console.log(this.props.list)
    const context = ContextSummoner.get;
    console.log(this.props.path)
  }

  getFolderName(path) {
    let cursor = path.lastIndexOf("/");
    return path.substring(cursor + 1, path.length)
  }

  getPathBack(path) {
    let cursor = path.lastIndexOf("/");
    let fpath = path.substring(0, cursor);
    if (fpath == "")
      return "/"
    return fpath;
  }

  addFolder() {
    var name = prompt("Folder name", "New Folder");

    if (name != null) {
      if (this.props.path.endsWith("/"))
        location.href = `/?path=${this.props.path}${name}`;
      else
        location.href = `/?path=${this.props.path}/${name}`;
    }
  }

  render() {
    const context = ContextSummoner.get;
    return (
      <Layout>

        <Head>
          <title>Bookmarks</title>
        </Head>
        {/* 
        <div className={css.add_bar}>
          <form action={`/addurl/?path=${this.props.path}`} method="post">
            <input className={css.textbox} type="text" name="url" placeholder={this.props.path}/>
            <input className={css.button} type="submit" value="Add" /> 
          </form> 
        </div> */}

        <FullScreen ref={r => this.addFs = r}>
          <FullScreenBackground type="dim" onClick={() => this.addFs.hide()}>
          </FullScreenBackground>
          <div className={css.add_popup}> 
              <form action={`/addurl/?path=${this.props.path}`} method="post">
                <input className={css.textbox} type="text" name="url" placeholder={this.props.path} />
                <input className={css.button} type="submit" value="Add" />
              </form> 
          </div>
        </FullScreen>

        <div className={css.address_bar}>
          {this.props.path}
        </div>
        <div className={css.side_bar}>
          <div onClick={() => this.addFs.show()} className={css.side_bar_btn}>
            <i className={`fas fa-plus`} />
          </div>
          <div onClick={() => this.addFolder()} className={css.side_bar_btn}>
            <i className={`fas fa-folder-plus`} />
          </div>
          {/* <form action="/logout" method="post">
            <button type="submit" className={css.side_bar_btn}>
              <i className={`fas fa-sign-out-alt`} />
            </button>
          </form> */}
        </div>
        <div className={css.main_body}>
          <div className={css.folder_container}>
            {
              this.props.path != "/" ?
                <FolderTile url={`/?path=${this.getPathBack(this.props.path)}`} name={"..."} />
                : null
            }
            {
              this.props.subDirs.map((e) => {
                return (
                  <FolderTile url={`/?path=${e}`} name={this.getFolderName(e)} />
                )
              })
            }
          </div>
          <div className={css.container}>
            {this.props.urls.map(url => {
              return (
                <div className={css.split3}>
                  <UrlTile url={url} />
                </div>
              )
            })}
          </div>
        </div>

      </Layout>
    );
  }
}


