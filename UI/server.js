const express = require('express')
const next = require('next')
const settings = require("./next.config");

const dev = process.env.NODE_ENV !== 'production'
const port = (dev) ? settings.portDev : settings.portProduction
const app = next({ dev })
const handle = app.getRequestHandler()

var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser') 

async function main() {
  
  try {
    await app.prepare()
  } catch (ex){
    console.error(ex.stack)
    process.exit(1)
  }

  const server = express()
  server.use(bodyParser.json()); // support json encoded bodies
  server.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
  server.use(cookieParser())

  server.use('/media', express.static('/media'))

  let routes = await require("./routes.js")(app, server);

  routes.forEach(route => {
    server.get(route.pattern, route.handler)
  });

  server.get('*', (req, res) => { 
    return handle(req, res)
  })

  server.listen(port, (err) => {
    if (err) throw err
    console.log('> Ready on http://localhost:' + port)
  })
}

main()