
 
const ObjectID = require('mongodb').ObjectID;
 
module.exports = async (req, res, db)=>{
   
    let authenticated = false; 
    let user = null; 
    let session = null;

    if (req.cookies != undefined){
      if (req.cookies.session){
        
        let session = ( await db.collection('session').find({_id: ObjectID(req.cookies.session) }).toArray() )[0];
 
        if (session){ 
          user = ( await db.collection('auth_user').find({_id:session.user }).toArray() )[0];
          session = session;
          user = user;
          authenticated = true;
          
        }
 
      }
    }
    return { authenticated, user, session };
 

}