
var pbkdf2 = require('pbkdf2-sha256');
var validatePassword = function (key, string) {
    var parts = string.split('$');
    var iterations = parts[1];
    var salt = parts[2];
    return pbkdf2(key, new Buffer(salt), iterations, 32).toString('base64') === parts[3];
};

module.exports = async (req, res, db)=>{
   
    if (req.cookies != undefined  ){
      if (req.cookies.session){

        await db.collection('session').deleteOne({_id:req.cookies.session})
        res.clearCookie("session"); 
        
      }
    }
    res.redirect('/'); 

}