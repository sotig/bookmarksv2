const ObjectID = require("mongodb").ObjectID

module.exports = async (req, res, db) => {
 
  await db.collection('url').deleteOne({
    _id: ObjectID(req.query.id)
  })

  res.redirect(`/?path=${req.query.path}`)

}