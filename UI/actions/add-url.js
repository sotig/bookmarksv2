const  saveSnapShotAndTitle = require('../core/url-handler')

module.exports = async (req, res, db) => {

  let data = await saveSnapShotAndTitle(req.body.url);
  let title = data.title
  let thumb = data.thumb
   
  await db.collection('url').insertOne({
    "createdAt": new Date(),
    "lastThumb": new Date(),
    "url": req.body.url,
    "thumb": thumb,
    "title": title,
    "path": req.query.path,
  })

  res.redirect(`/?path=${req.query.path}`)

}