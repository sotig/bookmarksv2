const saveSnapShotAndTitle = require('../core/url-handler')
const moment = require('moment')
const ObjectID = require('mongodb').ObjectID

async function updateUrl(urlEntry, db, id) {
    await db.collection('url').updateOne( 
        { "_id": ObjectID(id) }, 
        { 
            $set: { 
                "lastThumb":  new Date(), 
            }
        }, 
        {
            upsert: false
        }
    )
    saveSnapShotAndTitle(urlEntry.url);
}

async function updateClicks(urlEntry, db, id) {
    await db.collection('url').updateOne( 
        { "_id": ObjectID(id) }, 
        { 
            $set: {  
                "clicks": (urlEntry.clicks || 0) + 1
            }
        }, 
        {
            upsert: false
        }
    ) 
}


module.exports = async (req, res, db) => {
 
    let id = req.query.id
    if (id) {
        let urlEntry = await db.collection('url').findOne({ _id: ObjectID(id) })
        if (urlEntry) {
            if (!urlEntry.lastThumb) {
                updateUrl(urlEntry, db, id)
            } else {
                let lastThumb = moment(urlEntry.lastThumb)
                let today = moment(new Date)
                if (today.diff(lastThumb, 'days') >= 3) {
                    updateUrl(urlEntry, db, id)
                }
            }
            await updateClicks(urlEntry, db, id)
        } 
    } 

    res.end('ok')

}