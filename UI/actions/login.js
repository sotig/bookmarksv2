
var pbkdf2 = require('pbkdf2-sha256');
var validatePassword = function (key, string) {
  var parts = string.split('$');
  var iterations = parts[1];
  var salt = parts[2];
  return pbkdf2(key, new Buffer(salt), iterations, 32).toString('base64') === parts[3];
};

module.exports = async (req, res, db) => {
  const user = (await db.collection('auth_user').find({ username: req.body.username }).toArray())[0];
  if (!user) {
    res.send("wrong username");
    return;
  }

  let session = (await db.collection('session').find({ user: user._id }).toArray())[0];

  if (req.cookies != undefined && session != undefined) {
    if (req.cookies.session == session._id.toString()) {
      res.send("already autheticated");
      return;
    }
    else {
      res.clearCookie("session");
    }
  }

  if (user) {
    if (session) {
      await db.collection('session').deleteOne({ _id: session._id })
    }
    if (validatePassword(req.body.password, user.password)) {
      //await db.collection('session').createIndex({ "createdAt": 1 }, { expireAfterSeconds: 86400 * 30 });
      session = await db.collection('session').insertOne({
        "createdAt": new Date(),
        "user": user._id
      });

      session = session.ops[0];

      res.cookie("session", session._id.toString());

      res.redirect('/');
    } else { 
      res.send("wrong password");
    }

  }

}