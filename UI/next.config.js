const dev = process.env.NODE_ENV !== 'production'
const withSass = require('@zeit/next-sass')
const withCSS = require('@zeit/next-css') 

const nextConfig = {
  useFileSystemPublicRoutes: false,
  portDev: 3000,
  portProduction: 3000,
};

const withCustomBabel = (nextConfig) => {
  return Object.assign({}, nextConfig, {
    webpack(config, options) {
      // Error in fs.realpath
      // fs fix --> related to (Module not found: Error: Can't resolve 'fs' in '/node_modules/fs.realpath')
      // config.node = { fs: 'empty' };
      // config.target = 'node';        

      config.module.rules.push({
        test: /\.(wasm)$/i,
        type: 'javascript/auto',
        loader: 'file-loader',
        options: {
          publicPath: '/static/wasm/',
          outputPath: '../../static/wasm/',
          name: '[name].[ext]',
        },
      })

      config.module.rules.push({
        test: /~.*\.(js)$/i,
        type: 'javascript/auto',
        loader: 'file-loader',
        options: {
          publicPath: '/static/js/',
          outputPath: '../../static/js/',
          name: '[name].[ext]',
        },
      })
 

      // config.module.rules.push({
      //   // Include ts, tsx, js, and jsx files.
      //   test: /\.(ts)x?$/,
      //   exclude: /node_modules/,
      //   loader: 'babel-loader',
      // })
 

      if (typeof nextConfig.webpack === 'function') {
        return nextConfig.webpack(config, options)
      }

      return config
    }
  })
}

let settings =
  withSass(withCustomBabel({

    ...withCSS({ cssModules: false }),
    cssModules: true,
    cssLoaderOptions: {
      //localIdentName: (dev)?"[name]__[local]___[hash:base64:128]":undefined,
      localIdentName: (dev) ? "[local]____[hash:base64:32]" : undefined,
    }

  }))

settings["useFileSystemPublicRoutes"] = false;
settings["portDev"] = 3000;
settings["portProduction"] = 3000;


module.exports = settings


