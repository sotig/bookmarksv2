const dev = process.env.NODE_ENV !== 'production'
let globals = { 
    dev,
    dbUrl: "mongodb://mongo:27017",
    dbName: "snw_db" 

}

module.exports = globals