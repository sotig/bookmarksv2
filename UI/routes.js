

let routerExtendApp = require("./core/routing/router-extend-app"); 
const globals = require("./globals")
const { MongoClient } = require('mongodb')  

const login = require("./actions/login");
const logout = require("./actions/logout");
const addUrl = require("./actions/add-url");
const deleteUrl = require("./actions/delete-url");
const tests = require("./actions/tests");
const handleClick = require("./actions/handle-click");

module.exports = ( app, server )=> {
  return new Promise(resolve=>{

    
    MongoClient.connect( globals.dbUrl + globals.dbName, { useNewUrlParser: true }).then(client=>{
      routerExtendApp( app );
      
      const db =client.db(globals.dbName);
        let routes = [
  
        app.createPath("/", "index", null),
        {
          pattern:"/tests",
          handler: async (req, res) => tests(req, res, db),
        }
        //app.createPath("/:id", "index", (req, res)=>{ req.query["test"] = req.params.id }),
  
      ]
  
      server.post('/login', async (req, res) => login(req, res, db) )
      server.post('/logout', async (req, res) => logout(req, res, db) )
      server.post('/addurl', async (req, res) => addUrl(req, res, db) )
      server.post('/deleteurl', async (req, res) => deleteUrl(req, res, db) )
      server.get('/handle-click', async (req, res) => handleClick(req, res, db) )
        
      resolve(routes);
    });
    
  });

 
};